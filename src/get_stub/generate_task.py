"""
Для генерации DOS заглушки нужно положить эти скрипты рядом с FASM.exe,
и создать следующие каталоги
.\\generate\\asm\\
.\\generate\\bin\\
"""
from template_second import second_str
from template_first import first_str
import subprocess as sub
import random
#PATH_TO_ASM = "D:\\ASM\\fasmw17327\\FASM.exe"
PATH_ASM_FILES_GENERATE = ".\\generate\\asm\\"
PATH_BIN_FILES_GENERATE = ".\\generate\\bin\\"
COUNT_STUB = 100
flag = 'mctf{4r3_y0u_r3mb3r_d05?d1630cu}'
fake = 'mctf{f4k3_fl46!_c4n_y0u_f1nd_??}'
base_str = "This program cannot be run in DOS mode"
flag_id = 65
enc_keys = [random.randint(33, 127) for _ in range(COUNT_STUB)]
linear_operands = [random.randint(0,2) for _ in range(COUNT_STUB)]
items = [ random.randint(33, 100) if(linear_operands[i] == 0) else random.randint(3, 32) for i in range(COUNT_STUB)]


def add_null(data):
    if(len(data) == 1):
        return "00" + data
    if(len(data) == 2):
        return "0" + data
    if(len(data) == 3):
        return data
#sec_filename = "test_sec.asm"
#with open(sec_filename,"wt") as f:
#    f.write(second_str.format(add_number = 15))
#process = sub.run(f".\FASM.exe {sec_filename}")
#print(items)
index = 0
gamma_data = ''
gamma_bytes = []
for add_sym in items:
    enc_data = ''
    for i in range(0,32):
        if ( index == flag_id ):
            if (linear_operands[index] == 0):
                enc_data += chr( (ord(base_str[i]) + add_sym) ^ ord(flag[i]) )
            else:
                enc_data += chr( (ord(base_str[i]) - add_sym) ^ ord(flag[i]) )
        else:
            if (linear_operands[index] == 0):
                enc_data += chr( (ord(base_str[i]) + add_sym) ^ ord(fake[i]) )
            else:
                enc_data += chr( (ord(base_str[i]) - add_sym) ^ ord(fake[i]) )
    gamma_bytes.append(enc_data)
    hex_data = ''
    for i in enc_data:
        hex_data += (add_null(hex(ord(i))[2:]) + 'h,')
    #print(hex_data)
    index += 1
    gamma_data += hex_data
def TestDecryptFlag(base_str,enc_flag,add_sym):
    answ = ''
    for i in range(0,32):
        if (linear_operands[flag_id] == 0):
            answ += chr(ord(enc_flag[i]) ^ (ord(base_str[i]) + add_sym))
        else:
            answ += chr(ord(enc_flag[i]) ^ (ord(base_str[i]) - add_sym))
    print(answ, answ == flag)
#print(len(set(gamma_bytes)))
gamma_data = gamma_data[:-1]
enc_flag = gamma_data[flag_id * 160:flag_id * 160 + 159]
enc_flag = [chr(int(sym[1:-1],16)) for sym in enc_flag.split(',')]
TestDecryptFlag(base_str,enc_flag,items[flag_id])
TestDecryptFlag(base_str,gamma_bytes[flag_id],items[flag_id])

def GenerateEncCode(items,enc_keys):
    res = ''
    res_mas = []
    adds_and_keys = zip(items,enc_keys)
    index = 0
    len_one_stub = 0
    for add_sym,key in adds_and_keys:
        second_filename = "test_sec" + str(index) + ".asm"
       # print(second_filename[:-3]+"exe")
        
        with open(PATH_ASM_FILES_GENERATE + second_filename,"wt") as f:
            if (linear_operands[index] == 0):
                f.write(second_str.format(linear_operand = "add",add_number = add_sym))
            else:
                f.write(second_str.format(linear_operand = "sub",add_number = add_sym))
        process = sub.run(f".\FASM.exe {PATH_ASM_FILES_GENERATE}{second_filename}")
        enc_data = ''
        #original_data = ''
        index+=1
        #print(second_filename[:-3]+"exe")
        with open( PATH_ASM_FILES_GENERATE + second_filename[:-3]+"exe","rb") as f:
            d = f.read(0xA4)
            filedata = f.read()
            raw_byte = filedata
            len_one_stub = len(raw_byte)
            enc_byte = ''
            for i in filedata:
                #original_data += (add_null(hex(i)[2:]) + 'h,')
                enc_data += (add_null(hex(i ^ key)[2:]) + 'h,')
                enc_byte += chr(i ^ key)
        res += enc_data
        res_mas.append(enc_byte)
    res = res[:-1]
    return res,res_mas,len_one_stub
all_enc_data,all_enc_mas,len_one_stub = GenerateEncCode(items,enc_keys)

def GenerateFinalyTask(count_stub,len_one_stub,gamma_data,enc_data,filename):
    task_asm = first_str.format(count_stub = count_stub,len_one_stub = len_one_stub,gamma_data = gamma_data,enc_data = enc_data)
    filename = filename + ".asm"
    with open(PATH_BIN_FILES_GENERATE + filename,"wt") as f:
        f.write(task_asm)
    process = sub.run(f".\FASM.exe {PATH_BIN_FILES_GENERATE}{filename}")
        
    
GenerateFinalyTask(COUNT_STUB,
                   len_one_stub,
                   gamma_data,
                   all_enc_data,
                   "STUB")
GenerateFinalyTask(1,
                   len_one_stub,
                   gamma_data[flag_id * 160:flag_id * 160 + 159],
                   (''.join([add_null(hex(ord(i))[2:]) + 'h,' for i in all_enc_mas[flag_id]]))[:-1],
                   "TEST")
print("Key for enc_data is {}".format(chr(enc_keys[flag_id])))
with open('keys.csv','wt') as f:
    f.write(f"id,key\n")
    index = 0
    for i in enc_keys:
        sym = chr(i)
        opp = "add" if(linear_operands[index] == 0) else "sub"
        f.write(f"{index},{sym}\n")
        index += 1
